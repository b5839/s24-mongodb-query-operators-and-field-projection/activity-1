/*
4. Find users with the letter e in their first name and have an age of less than or equal to 30.
Use the $and, $regex and $lte operators
5. Create a git repository named S24.
6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
7. Add the link in Boodle.
*/

db.users.find({$or: [
    {"firstName": {$regex: "s", $options: "$i"}},
    {"lastName ": {$regex: "d", $options: "$i"}}
] },{
        "firstName": 1,
        "lastName": 1,
        "_id": 0
    }
);

db.users.find({$and: [{"age" : {$gt: 70}},{"age": {$gte:70}}]});
db.users.find({$and: [{"firstName": {$regex: "e", $options: "$i"}}, {"age": {$lte: 30} }]});








